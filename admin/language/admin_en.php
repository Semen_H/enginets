<?php
    return [
        "titles"=>[
            "login.php"=>"Login",
            "admin.php"=>"Home | Admin panel",
            "engine_orders.php"=>"������ | ������ ��������������",
            "engine_products.php"=>"������ | ������ ��������������",
            "add_product.php"=>"�������� ����� | ������ ��������������"
        ],

        "login.php"=>[
            "Auth"=>"������� � ������� ��� ����������� ������",
            "login_placeholder"=>"������� �����",
            "password_placeholder"=>"������� ������",
            "login_button"=>"�����",

        ],

        "footer.php"=>[
            "copy"=>"Developed by Semen Gogolev &copy; 2016",
        ],

        "header.php"=>[
            "greeting"=>"������������,",
            "see_site"=>"����������� ����",
            "logout"=>"�����",
            "menu_h"=>"����",
            "menu_home"=>"�������",
            "menu_products"=>"������",
            "menu_orders"=>"������",
            "menu_add_product"=>"�������� �����",
            "Correct_login_password"=>"�������� ��� ������������ �/��� ������",
            "is_empty"=>"������� ����� �/��� ������",
            "select_one_order"=>"�������� ���� �� ���� �����!",
            "confirm_delete_orders"=>"�� �������? ����� ������� ������ � id:",
            "confirm_delete_products"=>"�� �������? ����� ������� ������ � id:",
            "select_one_product"=>"�������� ���� �� ���� �����!",
        ],

        "admin.php"=>[
            "h1"=>"�������",
            "last_orders_text"=>"��������� ������ - ��������: ",
            "last_products_text"=>"��������� ����������� ������ - ��������: ",
            "last_orders_table_name"=>"���",
            "last_orders_table_phone"=>"�������",
            "last_orders_table_adress"=>"����� ��������",
            "last_orders_table_comments"=>"�����������",
            "last_orders_table_product_id"=>"��� ������",
            "last_orders_table_order_id"=>"ID ������",
            "last_orders_table_date"=>"���� ������",
            "last_orders_table_status"=>"������",
            "last_orders_table_look_order"=>"���������� ����� id=",
            "last_orders_table_look_all_order"=>"���������� ��� ������",
            "product_img_text"=>"����������� ������",
            "product_name_text"=>"�������� ������",
            "product_id_text"=>"��� ������",
            "product_description_text"=>"�������� ������",
            "product_price_text"=>"���� ������",
            "look_product_text"=>"������������� ����� id=",
            "title_edit_product_text"=>"������������� �����",
            "look_all_products_text"=>"���������� ��� ������",
            "all_orders_text"=>"| ����� -",
            "all_products_text"=>"| ����� -"
        ],

        "engine_orders.php"=>[
            "h1"=>"������",
            "all_orders_text"=>"��� ������:",
            "last_orders_table_name"=>"��� �������",
            "last_orders_table_phone"=>"������� �������",
            "last_orders_table_adress"=>"����� ��������",
            "last_orders_table_comments"=>"�����������",
            "last_orders_table_product_id"=>"��� ������",
            "last_orders_table_order_id"=>"ID ������",
            "last_orders_table_date"=>"���� ������",
            "last_orders_table_status"=>"������",
            "last_orders_table_look_order"=>"���������� ����� id=",
            "count_post_per_page"=>"| �� �������� ��������: ",
            "page_per"=>" | ��������:"
        ],

        "engine_products.php"=>[
            "h1"=>"������",
            "product_img_text"=>"����������� ������",
            "product_name_text"=>"�������� ������",
            "product_id_text"=>"ID ������",
            "product_description_text"=>"�������� ������",
            "product_price_text"=>"���� ������",
            "all_products_text"=>"��� ������:",
            "title_edit_product_text"=>"������������� �����",
            "look_product_text"=>"������������� ����� id=",
            "count_post_per_page"=>"| �� �������� ��������: ",
            "page_per"=>" | ��������:",
            "add_product_text"=>"�������� �����"
        ],

        "section_pagination"=>[
            "first_page_text"=>"������ ��������",
            "previous_page_text"=>"���������� ��������",
            "current_page_text"=>"������� ��������",
            "next_page_text"=>"��������� ��������",
            "last_page_text"=>"��������� ��������"
        ],

        "section_options"=>[
            "select_all_text"=>" ������� ��",
            "confirm_button_text"=>"������� ����������"
        ],

        "section_sys_messages"=>[
            "system_message_id_1"=>"��������� ������ ���� ������� �������: ",
            "system_message_id_2"=>"������������ ������ � ���� ������",
            "system_message_id_3"=>"��������� ������ ���� ������� �������: ",
            "system_message_id_4"=>"������������ ������ � ���� ������"
        ],

        "add_product.php"=>[
            "h1"=>"�������� �����",
            "fill_form"=>"��������� ����� ��� ���������� ������"
        ]
    ];
?>