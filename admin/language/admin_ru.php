<?php
    return [
        "titles"=>[
            "login.php"=>"Логин",
            "admin.php"=>"Главная | Панель администратора",
            "engine_orders.php"=>"Заказы | Панель администратора",
            "engine_products.php"=>"Товары | Панель администратора",
            "add_product.php"=>"Добавить товар | Панель администратора",
            "product.php"=>"Редактировать товар | Панель администратора"
        ],

        "login.php"=>[
            "Auth"=>"Войдите в систему для продолжения работы",
            "login_placeholder"=>"Введите логин",
            "password_placeholder"=>"Введите пароль",
            "login_button"=>"Войти",

        ],

        "footer.php"=>[
            "copy"=>"Developed by Semen Gogolev &copy; 2016",
        ],

        "header.php"=>[
            "greeting"=>"Здравствуйте,",
            "see_site"=>"Просмотреть сайт",
            "logout"=>"Выйти",
            "menu_h"=>"Меню",
            "menu_home"=>"Главная",
            "menu_products"=>"Товары",
            "menu_orders"=>"Заказы",
            "menu_add_product"=>"Добавить товар",
            "Correct_login_password"=>"Неверные имя пользователя и/или пароль",
            "is_empty"=>"Введите логин и/или пароль",
            "select_one_order"=>"Выберите хотя бы один заказ!",
            "confirm_delete_orders"=>"Вы уверены? Будут удалены заказы с id:",
            "confirm_delete_products"=>"Вы уверены? Будут удалены товары с id:",
            "select_one_product"=>"Выберите хотя бы один товар!",
        ],

        "admin.php"=>[
            "h1"=>"Главная",
            "last_orders_text"=>"Последние заказы - показано: ",
            "last_products_text"=>"Последние добавленные товары - показано: ",
            "last_orders_table_name"=>"Имя",
            "last_orders_table_phone"=>"Телефон",
            "last_orders_table_adress"=>"Адрес доставки",
            "last_orders_table_comments"=>"Комментарий",
            "last_orders_table_product_id"=>"Код товара",
            "last_orders_table_order_id"=>"ID заказа",
            "last_orders_table_date"=>"Дата заказа",
            "last_orders_table_status"=>"Статус",
            "last_orders_table_look_order"=>"Посмотреть заказ id=",
            "last_orders_table_look_all_order"=>"Посмотреть все заказы",
            "product_img_text"=>"Изображение товара",
            "product_name_text"=>"Название товара",
            "product_id_text"=>"Код товара",
            "product_description_text"=>"Описание товара",
            "product_price_text"=>"Цена товара",
            "look_product_text"=>"Редактировать товар id=",
            "title_edit_product_text"=>"Редактировать товар",
            "look_all_products_text"=>"Посмотреть все товары",
            "all_orders_text"=>"| Всего -",
            "all_products_text"=>"| Всего -"
        ],

        "engine_orders.php"=>[
            "h1"=>"Заказы",
            "all_orders_text"=>"Все заказы:",
            "last_orders_table_name"=>"Имя клиента",
            "last_orders_table_phone"=>"Телефон клиента",
            "last_orders_table_adress"=>"Адрес доставки",
            "last_orders_table_comments"=>"Комментарий",
            "last_orders_table_product_id"=>"Код товара",
            "last_orders_table_order_id"=>"ID заказа",
            "last_orders_table_date"=>"Дата заказа",
            "last_orders_table_status"=>"Статус",
            "last_orders_table_look_order"=>"Посмотреть заказ id=",
            "count_post_per_page"=>"| На страницу выводить: ",
            "page_per"=>" | Страница:"
        ],

        "engine_products.php"=>[
            "h1"=>"Товары",
            "product_img_text"=>"Изображение товара",
            "product_name_text"=>"Название товара",
            "product_id_text"=>"ID товара",
            "product_description_text"=>"Краткое описание товара",
            "product_price_text"=>"Цена товара",
            "all_products_text"=>"Все товары:",
            "title_edit_product_text"=>"Редактировать товар",
            "look_product_text"=>"Редактировать товар id=",
            "count_post_per_page"=>"| На страницу выводить: ",
            "page_per"=>" | Страница:",
            "add_product_text"=>"Добавить товар",
            "product_articul_text"=>"Артикул"
        ],

        "section_pagination"=>[
            "first_page_text"=>"Первая страница",
            "previous_page_text"=>"Предыдущая страница",
            "current_page_text"=>"Текущая страница",
            "next_page_text"=>"Следующая страница",
            "last_page_text"=>"Последняя страница"
        ],

        "section_options"=>[
            "select_all_text"=>" Выбрать всё",
            "confirm_button_text"=>"Удалить отмеченные"
        ],

        "section_sys_messages"=>[
            "system_message_id_1"=>"Следующие заказы были успешно удалены: ",
            "system_message_id_2"=>"Отстутствуют заказы в базе данных",
            "system_message_id_3"=>"Следующие товары были успешно удалены: ",
            "system_message_id_4"=>"Отстутствуют товары в базе данных",
            "system_message_id_5"=>"Был успешно добавлен товар id: ",
            "system_message_id_6"=>"Ошибка! Товар не был добавлен. Попробуйте снова.",
            "system_message_id_7"=>"Ошибка добавления изображения! Загружаемый файл не является допустимым. Только .jpg/.jpeg или .png",
            "system_message_id_8"=>"Ошибка добавления изображения! Загружаемый файл превышает допустимый размер",
            "system_message_id_9"=>"Данный товар отсутствует в базе данных! Проверьте URL",
            "system_message_id_10"=>"Товар с указанным id был успешно удален:",
        ],

        "add_product.php"=>[
            "h1"=>"Добавить товар",
            "fill_form"=>"Заполните форму для добавления товара",
            "product_name_text"=>"Название товара*",
            "product_articul_text"=>"Артикул товара*",
            "help_name_text"=>"Минимум 10 символов. Макс.254 символа",
            "help_articul_text"=>"Артикул товара должен быть уникальным! Минимум 4 символа. Макс.254 символа",
            "title_text"=>"Title",
            "meta_descr_text"=>"Meta Description",
            "meta_keywords_text"=>"Meta Keywords",
            "help_keywords_text"=>"Перечислите ключевые слова через запятую",
            "product_price_text"=>"Цена*",
            "help_pr_price_text"=>"Минимум 2 символа. Макс. 254",
            "upl_file_text"=>"Загрузить изображение(.jpg, .jpeg, .png, .gif)&le;2Мб",
            "help_upl_file_text"=>"Если вы не выберите изображение, то оно будет подставлено по умолчанию",
            "product_br_description"=>"Краткое описание товара*",
            "min_br_text"=>"Минимум 20 символов.",
            "full_descr_text"=>"Полное описание товара*",
            "min_full_text"=>"Минимум 40 символов.",
            "add_product_text"=>"Добавить товар"
        ],

    ];
?>