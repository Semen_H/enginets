switch(system_language_prefix){
    case "ru":
        tinymce.init({
            selector: '#product_br_descr_textarea, #product_full_descr_textarea',
            plugins: 'code',
            language: 'ru'
        });
    break;
    case "en":
        tinymce.init({
            selector: '#product_br_descr_textarea, #product_full_descr_textarea',
            plugins: 'code',
        });
    break;
}

$(document).ready(function(){
    var
        errors=true,

        /*/////////////////////////////
                    ������
        */////////////////////////////

        //�������� ������
        product_name_class=$('.product_name'),
        help_name=$('.help-product_name'),
        wrapper_name=$('.name_wrapper'),

        //������� ������
        articul_wrapper=$('.articul_wrapper'),
        help_articul=$('.help-articul'),
        articul_class=$('.product_articul'),

        //���� ������
        help_price=$('.help-price'),
        price_wrapper=$('.price-wrapper'),
        price_class=$('.product_price'),

        //������� ��������
        help_br_descr=$('.help-br_descr'),

        //������ ��������
        help_full_descr=$('.help-full_descr');

/*//////////////////////////////////
        ��������� �����
*//////////////////////////////////
$('.add_product_form').on("submit", function(){

    //������� html ����
    String.prototype.stripTags = function() {
        return this.replace(/<\/?[^>]+>/g, '');
    };


   var
    //�������� value
    product_name=$('.product_name').val(),//�������� ������
    articul=$('.product_articul').val(),//������� ������
    product_title=$('.product_title').val(),//seo title
    seo_description=$('.seo_description').val(),//seo description
    seo_keywords=$('.seo_keywords').val(),//seo keywords
    price=$('.product_price').val(),//���� ������
    product_br_descr=$('#product_br_descr_textarea_ifr').contents().find('body').html().stripTags(),//������� ��������
    product_full_descr=$('#product_full_descr_textarea_ifr').contents().find('body').html().stripTags(),//������ ��������

    //������� �������
    count_product_name=product_name.length,//���������� �������� � ����� ������
    count_articul=articul.length,//���������� �������� � �������
    count_product_price=price.length,//���������� �������� � ����
    count_product_br_descr=product_br_descr.length,//���������� �������� � ������� ��������
    count_product_full_descr=product_full_descr.length;//���������� �������� � ������ ��������

    //�������� �������� ������
    if(count_product_name < 10 || count_product_name > 255){
        help_name.addClass("bg-danger");
        wrapper_name.addClass("has-error");
    }

    //�������� ��������
    if(count_articul < 3 || count_articul > 255){
        help_articul.addClass("bg-danger");
        articul_wrapper.addClass("has-error");
    }

    //�������� ���� ������
    if(count_product_price < 2 || count_product_price > 255){
        help_price.addClass("bg-danger");
        price_wrapper.addClass("has-error");
    }

    //�������� �������� �������� ������
    if(count_product_br_descr < 20){
        help_br_descr.addClass("red");
    }

    //�������� ������� �������� ������
    if(count_product_full_descr < 40){
        help_full_descr.addClass("red");
    }

    if((count_product_name >= 10 || count_product_name < 255) && (count_articul >= 3 || count_articul < 255) && (count_product_price >= 2 || count_product_price < 255) && count_product_br_descr >= 20 && count_product_full_descr >= 40){
        return true;
    }
    else return false;

});

/*//////////////////////////////////
       ������� ������� �� �����
*//////////////////////////////////

//�������� ������
    product_name_class.on("click", function(){
        if(help_name.hasClass("bg-danger")){
            help_name.removeClass("bg-danger");
        }
        if(wrapper_name.hasClass("has-error")){
            wrapper_name.removeClass("has-error");
        }
    });

//������� ������
    articul_class.on("click", function(){
        if(articul_wrapper.hasClass("has-error")){
            articul_wrapper.removeClass("has-error");
        }
        if(help_articul.hasClass("bg-danger")){
           help_articul.removeClass("bg-danger");
        }
    });

//���� ������
    price_class.on("click", function(){
        if(price_wrapper.hasClass("has-error")){
            price_wrapper.removeClass("has-error");
        }
        if(help_price.hasClass("bg-danger")){
           help_price.removeClass("bg-danger");
        }
    });

});