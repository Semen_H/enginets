switch(system_language_prefix){
    case "ru":
        tinymce.init({
            selector: '#product_br_descr_textarea, #product_full_descr_textarea',
            plugins: 'code',
            language: 'ru'
        });
    break;
    case "en":
        tinymce.init({
            selector: '#product_br_descr_textarea, #product_full_descr_textarea',
            plugins: 'code',
        });
    break;
}

$(document).ready(function(){
/******************************
    Удаление товара по кнопке
******************************/
    //fancybox init
    $(".fancybox").fancybox();

    /*
        Показываем мессагу и редиректим на страницу товаров.
    */
    function success_function(){
        alert("Вы успешно удалили товар! Сейчас вы будете перенаправлены на страницу товаров.");
        var product_id=current_url.split("=");
        document.location.href='/admin/controller/engine_products.php?page=1&show_message=true&messages_id=3&del_product_id='+product_id[1];
    }

    //delete product
    $(".mybtn-1").on("click", function(){
        var
            conf=confirm("Вы уверены?");

        if(conf==true){
            $.ajax({
                url: current_url,
                type: "POST",
                data: ({"delete_product": true}), //отправляем
                dataType: "html",
                success: success_function
           });
        }

    });
/*************************************
    END Удаление товара по кнопке
*************************************/

     var
        errors=true,

        /*/////////////////////////////
                    классы
        */////////////////////////////

        //название товара
        product_name_class=$('.product_name'),
        help_name=$('.help-product_name'),
        wrapper_name=$('.name_wrapper'),

        //артикул товара
        articul_wrapper=$('.articul_wrapper'),
        help_articul=$('.help-articul'),
        articul_class=$('.product_articul'),

        //цена товара
        help_price=$('.help-price'),
        price_wrapper=$('.price-wrapper'),
        price_class=$('.product_price'),

        //краткое описание
        help_br_descr=$('.help-br_descr'),

        //полное описание
        help_full_descr=$('.help-full_descr');

    $('.update_product_form').on("submit", function(){

        //убираем html теги
        String.prototype.stripTags = function() {
            return this.replace(/<\/?[^>]+>/g, '');
        };

        /*//////////////////////////////////
            Валидация формы
        *//////////////////////////////////
        var
            //получаем value
            product_name=$('.product_name').val(),//название товара
            articul=$('.product_articul').val(),//артикул товара
            product_title=$('.product_title').val(),//seo title
            seo_description=$('.seo_description').val()//seo description
            seo_keywords=$('.seo_keywords').val(),//seo keywords
            price=$('.product_price').val(),//цена товара
            product_br_descr=$('#product_br_descr_textarea_ifr').contents().find('body').html().stripTags(),//краткое описание
            product_full_descr=$('#product_full_descr_textarea_ifr').contents().find('body').html().stripTags(),//полное описание

            //считаем символы
            count_product_name=product_name.length,//количество символов в имени товара
            count_articul=articul.length,//количество символов в арткуле
            count_product_price=price.length,//количество символов в цене
            count_product_br_descr=product_br_descr.length,//количество символов в кратком описании
            count_product_full_descr=product_full_descr.length;//количество символов в полном описании

        //проверка названия товара
        if(count_product_name < 10 || count_product_name > 255){
            help_name.addClass("bg-danger");
            wrapper_name.addClass("has-error");
        }

        //проверка артикула
        if(count_articul < 3 || count_articul > 255){
            help_articul.addClass("bg-danger");
            articul_wrapper.addClass("has-error");
        }

        //проверка цены товара
        if(count_product_price < 2 || count_product_price > 255){
            help_price.addClass("bg-danger");
            price_wrapper.addClass("has-error");
        }

         //проверка краткого описания товара
        if(count_product_br_descr < 20){
            help_br_descr.addClass("red");
        }

        //проверка полного описания товара
        if(count_product_full_descr < 40){
            help_full_descr.addClass("red");
        }

        if((count_product_name >= 10 || count_product_name < 255) && (count_articul >= 3 || count_articul < 255) && (count_product_price >= 2 || count_product_price < 255) && count_product_br_descr >= 20 && count_product_full_descr >= 40){
            alert("Обновляем товар");
        }

        return false;

    });
/*//////////////////////////////////
       Убираем красный по клику
*//////////////////////////////////

//название товара
    product_name_class.on("click", function(){
        if(help_name.hasClass("bg-danger")){
            help_name.removeClass("bg-danger");
        }
        if(wrapper_name.hasClass("has-error")){
            wrapper_name.removeClass("has-error");
        }
    });

//артикул товара
    articul_class.on("click", function(){
        if(articul_wrapper.hasClass("has-error")){
            articul_wrapper.removeClass("has-error");
        }
        if(help_articul.hasClass("bg-danger")){
           help_articul.removeClass("bg-danger");
        }
    });

//цена товара
    price_class.on("click", function(){
        if(price_wrapper.hasClass("has-error")){
            price_wrapper.removeClass("has-error");
        }
        if(help_price.hasClass("bg-danger")){
           help_price.removeClass("bg-danger");
        }
    });

/*//////////////////////////////////
    Удаляем изображение по клику
*//////////////////////////////////
function reload_product_img(data){
    $('.img_in').html('<a href="'+default_img_url+'" class="fancybox" rel="gallery1"><img src="'+default_img_url+'" alt="" class="img-thumbnail loads" name="old_img"/></a>');
    $('.hide_if_default_img').hide();
}

$('.default_img_click #inlineCheckbox1').on('click', function() {
    setTimeout(function(){
        if ($('.default_img_click #inlineCheckbox1').is(':checked')) {
            var
                conf1=confirm("Вы уверены что хотите удалить изображение товара(будет установлено изображение по умолчанию)?");

            if(conf1==true){
                $.ajax({
                    url: current_url,
                    type: "POST",
                    data: ({"delete_img": true}), //отправляем
                    dataType: "html",
                    success: reload_product_img
                });
            } else{
                $('.default_img_click #inlineCheckbox1').prop('checked', false);
            }
        }
    }, 1);
});

});