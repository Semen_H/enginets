<?php
    require_once "../model/Auth_Class.php";
    $user=Auth::getObject();
    $auth=$user->isAuth();
    if($auth){
        require_once "header.php";
        require_once "../model/add_product.php";

        $add_obj=new Add_Product();
        $db_product_obj=new DB_Add_Product();

        $product_name=$_POST["product_post_name"];//название товара
        $articul=$_POST["product_post_articul"];//артикул товара
        //$sef=$_POST["product_post_sef"];//ЧПУ
        $pr_title=$_POST["product_post_title_for_client"];//seo title
        $pr_meta_description=$_POST["product_post_meta_description"];//seo desription
        $keywords=$_POST["product_post_meta_keywords"];//seo keywords
        $price=$_POST["product_post_price"];//цена товара
        $product_br_description=$_POST["product_br_descr_textarea"];//краткое описание товара
        $product_full_description=$_POST["product_full_descr_textare"];//описание товара

        $img_name=$_FILES["uploadfile"]["name"];//получаем имя изображения товара
        $tmp_name=$_FILES["uploadfile"]["tmp_name"];//временное имя изображения товара
        $file_type=$_FILES["uploadfile"]["type"];//myme-type загружаемого файла
        $file_syze=$_FILES["uploadfile"]["size"];//размер загружаемого файла
        $img_product_directory=$add_obj->getImgProductPatch();//путь к папке изображений товаров
        $max_img_size=2048000;//максимальный размер файла в байтах

        $error_msg=false;


        if($_POST){
            $count_product_name=iconv_strlen($product_name);//количество символов в названии товара
            $count_articul=iconv_strlen($articul);//количетсво символов в артикуле
            $count_price=iconv_strlen($price);//количетсво символов в артикуле
            $count_product_br_description=iconv_strlen(strip_tags($product_br_description));//количество символов в кратком описании
            $count_product_full_description=iconv_strlen(strip_tags($product_full_description));//количество символов в полном описании

            //добавление изображения
            if($img_name){ //если изображение прикреплено
                if(($file_type=="image/jpeg" or $file_type=="image/jpg" or $file_type=="image/png") and $file_syze < $max_img_size) {

                   $uploadfile=$img_product_directory.$img_name;
                   $rezult_add_img=move_uploaded_file($tmp_name, $uploadfile);

                   if(($count_product_name >= 10 and $count_product_name < 255) and ($count_articul >= 3 and $count_articul < 255) and ($count_price >= 2 and $count_price < 255) and $count_product_br_description >= 20 and $count_product_full_description >= 40 and $rezult_add_img){
                        $rezult_add=$db_product_obj->addProduct($product_name, $articul, $pr_title, $pr_meta_description, $keywords, $price, $product_br_description, $product_full_description, $img_name);
                        $regular_rezult=preg_match("/^\d{1,}$/", $rezult_add);
                        $errors=false;
                    }
                    else $errors=true;
                }

            }
            else{//если изображение не было прикреплено
                 if(($count_product_name >= 10 and $count_product_name < 255) and ($count_articul >= 3 and $count_articul < 255) and ($count_price >= 2 and $count_price < 255) and $count_product_br_description >= 20 and $count_product_full_description >= 40){
                    $rezult_add=$db_product_obj->addProduct($product_name, $articul, $pr_title, $pr_meta_description, $keywords, $price, $product_br_description, $product_full_description);
                    $regular_rezult=preg_match("/^\d{1,}$/", $rezult_add);
                    $errors=false;
                }
                else $errors=true;
            }

            if($regular_rezult==true and $errors==false){
                header("Location: /admin/controller/engine_products.php?page=1&show_message=true&messages_id=2&product_add_id=$rezult_add");
            }
            elseif($regular_rezult==false){
                $error_msg=true;
            }
        }



        /*///////////////////////////////////////////
                    Переменные локализации
        ///////////////////////////////////////////*/
        $h1=$add_obj->getWord("add_product.php", "h1");
        $fill_form=$add_obj->getWord("add_product.php", "fill_form");
        $system_message_id_6=$add_obj->getWord("section_sys_messages", "system_message_id_6");
        $system_message_id_7=$add_obj->getWord("section_sys_messages", "system_message_id_7");
        $system_message_id_8=$add_obj->getWord("section_sys_messages", "system_message_id_8");

        $product_name_text=$add_obj->getWord("add_product.php", "product_name_text");
        $product_articul_text=$add_obj->getWord("add_product.php", "product_articul_text");
        $help_name_text=$add_obj->getWord("add_product.php", "help_name_text");
        $help_articul_text=$add_obj->getWord("add_product.php", "help_articul_text");
        $title_text=$add_obj->getWord("add_product.php", "title_text");
        $meta_descr_text=$add_obj->getWord("add_product.php", "meta_descr_text");
        $meta_keywords_text=$add_obj->getWord("add_product.php", "meta_keywords_text");
        $help_keywords_text=$add_obj->getWord("add_product.php", "help_keywords_text");
        $product_price_text=$add_obj->getWord("add_product.php", "product_price_text");
        $help_pr_price_text=$add_obj->getWord("add_product.php", "help_pr_price_text");
        $upl_file_text=$add_obj->getWord("add_product.php", "upl_file_text");
        $help_upl_file_text=$add_obj->getWord("add_product.php", "help_upl_file_text");
        $br_descr_text=$add_obj->getWord("add_product.php", "product_br_description");
        $min_br_text=$add_obj->getWord("add_product.php", "min_br_text");
        $full_descr_text=$add_obj->getWord("add_product.php", "full_descr_text");
        $min_full_text=$add_obj->getWord("add_product.php", "min_full_text");
        $add_product_text=$add_obj->getWord("add_product.php", "add_product_text");


        require_once "../view/add_product.html";
        require_once "footer.php";
    }
    else{
        header("Location: login.php");
        exit;
    }
?>