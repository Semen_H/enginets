<?php
    require_once "../model/footer.php";

    $footer_obj=new Footer();
    $current_url=$footer_obj->getCurrUrl();

    /*///////////////////////////////////////////
            JS в зависимости от url
    ///////////////////////////////////////////*/
    switch($current_url){
        case "/admin/controller/login.php":
            $js_footer=[
                1=>[
                    "src"=>"../view/js/jquery_1.11.1.min.js"
                ],
                2=>[
                    "src"=>"../view/components/bootstrap-3.2.0-dist/js/bootstrap.min.js"
                ],
                3=>[
                    "src"=>"../view/js/main.js"
                ]
            ];
        break;
        case "/admin/controller/admin.php":
            $js_footer=[
                1=>[
                    "src"=>"../view/js/jquery_1.11.1.min.js"
                ],
                2=>[
                    "src"=>"../view/components/bootstrap-3.2.0-dist/js/bootstrap.min.js"
                ],

            ];
        break;
        case "/admin/controller/engine_orders.php":
            $js_footer=[
                1=>[
                    "src"=>"../view/js/jquery_1.11.1.min.js"
                ],
                2=>[
                    "src"=>"../view/components/bootstrap-3.2.0-dist/js/bootstrap.min.js"
                ],
                3=>[
                    "src"=>"../view/js/form_submit_engine_orders.js",
                ],
                4=>[
                    "src"=>"../view/js/select_all_checkbox.js",
                ]
            ];
        break;
        case "/admin/controller/engine_products.php":
            $js_footer=[
                1=>[
                    "src"=>"../view/js/jquery_1.11.1.min.js"
                ],
                2=>[
                    "src"=>"../view/components/bootstrap-3.2.0-dist/js/bootstrap.min.js"
                ],

                3=>[
                    "src"=>"../view/js/form_submit_engine_products.js",
                ],
                4=>[
                    "src"=>"../view/js/select_all_checkbox.js",
                ]
            ];
        break;
        case "/admin/controller/add_product.php":
            $js_footer=[
                1=>[
                    "src"=>"../view/js/jquery_1.11.1.min.js"
                ],
                2=>[
                    "src"=>"../view/components/bootstrap-3.2.0-dist/js/bootstrap.min.js"
                ],
                3=>[
                    "src"=>"../view/components/tinymce/tinymce.min.js"
                ],
                4=>[
                   "src"=>"../view/js/add_product.js"
                ]
            ];
        break;
        case "/admin/controller/product.php":
            $js_footer=[
                1=>[
                    "src"=>"../view/js/jquery_1.11.1.min.js"
                ],
                2=>[
                    "src"=>"../view/components/bootstrap-3.2.0-dist/js/bootstrap.min.js"
                ],
                3=>[
                    "src"=>"../view/components/tinymce/tinymce.min.js"
                ],
                5=>[
                    "src"=>"../view/components/fancyBox/lib/jquery.mousewheel-3.0.6.pack.js"
                ],
                6=>[
                    "src"=>"../view/components/fancyBox/source/jquery.fancybox.pack.js?v=2.1.5"
                ],
                7=>[
                    "src"=>"../view/components/fancyBox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"
                ],
                8=>[
                    "src"=>"../view/components/fancyBox/source/helpers/jquery.fancybox-media.js?v=1.0.6"
                ],
                9=>[
                    "src"=>"../view/components/fancyBox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"
                ],
                10=>[
                   "src"=>"../view/js/product.js"
                ]
            ];
        break;
    }


    /*///////////////////////////////////////////
                Переменные локализации
    ///////////////////////////////////////////*/
    $copy_text=$footer_obj->getWord("footer.php", "copy");

    require_once "../view/footer.html";
?>

