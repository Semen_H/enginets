<?php
    require_once "header.php";
    require_once "../model/login.php";
    require_once "../model/Auth_Class.php";

    $user1=Auth::getObject();
    $auth=$user1->isAuth();

    if(isset($_POST["auth"])){

        $login=$_POST["login"];
        $password=$_POST["password"];
        $auth_success=$user1->login($login, $password);
        if($auth_success){
            header("Location: admin.php");
            exit;
        }
        else $error_class="login_error";
    }
    $login_obj=new Login();
    /*///////////////////////////////////////////
                ���������� �����������
    ///////////////////////////////////////////*/
    $auth_text=$login_obj->getWord("login.php", "Auth");
    $login_placeholder_text=$login_obj->getWord("login.php", "login_placeholder");
    $password_placeholder_text=$login_obj->getWord("login.php", "password_placeholder");
    $login_button_text=$login_obj->getWord("login.php", "login_button");

    require_once "../view/login.html";
    require_once "footer.php";
?>

