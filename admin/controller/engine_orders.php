<?php
    require_once "../model/Auth_Class.php";
    $user=Auth::getObject();
    $auth=$user->isAuth();
    if($auth){
        $show_message=$_GET["show_message"];//отображать ли сообщения
        $msg_id=$_GET["messages_id"];//id сообщения

        require_once "header.php";
        require_once "../model/engine_orders.php";

        $orders_obj=new Engine_Orders();
        $db_admin_obj=new DB_Engine_Orders();
        $full_patch=$orders_obj->getFullPath();//получаем текущий полный url
        $cut_patch=$orders_obj->getCurrUrl();//получаем текущий сокращенный url

        $get_page=$_GET["page"];//получаем get-запрос

        $count_posts=10;//количество постов на страницу
        $is_number=preg_match("/^\d{1,}$/", $get_page);//число ли get-запрос
        $delete_array=$_POST["delete"];//получаем массив удаляемых заказов



        if($is_number){
            $count_all_posts_in_bd=$db_admin_obj->getCountAllOrders();//получаем количество заказов
            $array=$db_admin_obj->getAllOrders($get_page, $count_posts);//получаем все заказы
            $count_pages = ceil($count_all_posts_in_bd/$count_posts);//количество страниц общее.
            $pg=$get_page;//текущая страница


         //блок удаления записей
            if($delete_array) {//если что-либо есть в массиве(что удалять)
                session_start();
                $_SESSION["array_del_orders"]=$delete_array;


               $rezult_delete=$db_admin_obj->deleteOrders($delete_array);
               if($rezult_delete==true){ //если успешно удалили
                    header("Location: $cut_patch?page=1&show_message=true&messages_id=1");//редирект на первую страницу
               }
            }

            
            /* Входные параметры */

            $active = $get_page;//текущая активная страница.
            $count_show_pages = 5;// количество отображаемых страниц(визуально видимость)
            $url = "engine_orders.php";//адрес страницы, для которой и создаётся Pagination
            $url_page = "engine_orders.php?page=";//дрес страницы с параметром page без значения на конце

            if ($count_pages > 1) { // Всё это только если количество страниц больше 1
                /* Дальше идёт вычисление первой выводимой страницы и последней (чтобы текущая страница была где-то посредине, если это возможно, и чтобы общая сумма выводимых страниц была равна count_show_pages, либо меньше, если количество страниц недостаточно) */
                $left = $active - 1;
                $right = $count_pages - $active;
                if ($left < floor($count_show_pages / 2)) $start = 1;
                else $start = $active - floor($count_show_pages / 2);
                $end = $start + $count_show_pages - 1;
                if ($end > $count_pages) {
                    $start -= ($end - $count_pages);
                    $end = $count_pages;
                    if ($start < 1) $start = 1;
                }
            }
        }
        /*///////////////////////////////////////////
                    Переменные локализации
        ///////////////////////////////////////////*/
        $h1=$orders_obj->getWord("engine_orders.php", "h1");
        $all_orders_text=$orders_obj->getWord("engine_orders.php", "all_orders_text");
        $last_orders_text=$orders_obj->getWord("engine_orders.php", "last_orders_text");
        $name_text=$orders_obj->getWord("engine_orders.php", "last_orders_table_name");
        $phone_text=$orders_obj->getWord("engine_orders.php", "last_orders_table_phone");
        $adress_text=$orders_obj->getWord("engine_orders.php", "last_orders_table_adress");
        $comment_text=$orders_obj->getWord("engine_orders.php", "last_orders_table_comments");
        $product_id_text=$orders_obj->getWord("engine_orders.php", "last_orders_table_product_id");
        $order_id_text=$orders_obj->getWord("engine_orders.php", "last_orders_table_order_id");
        $date_text=$orders_obj->getWord("engine_orders.php", "last_orders_table_date");
        $status_text=$orders_obj->getWord("engine_orders.php", "last_orders_table_status");
        $look_order_text=$orders_obj->getWord("engine_orders.php", "last_orders_table_look_order");
        $count_post_per_page=$orders_obj->getWord("engine_orders.php", "count_post_per_page");
        $page_per=$orders_obj->getWord("engine_orders.php", "page_per");

        //секция опций
        $select_all_text=$orders_obj->getWord("section_options", "select_all_text");
        $confirm_button_text=$orders_obj->getWord("section_options", "confirm_button_text");

        //pagination
        $first_page_text=$orders_obj->getWord("section_pagination", "first_page_text");
        $previous_page_text=$orders_obj->getWord("section_pagination", "previous_page_text");
        $current_page_text=$orders_obj->getWord("section_pagination", "current_page_text");
        $next_page_text=$orders_obj->getWord("section_pagination", "next_page_text");
        $last_page_text=$orders_obj->getWord("section_pagination", "last_page_text");

        //messages
        $system_message_id_1=$orders_obj->getWord("section_sys_messages", "system_message_id_1");
        $system_message_id_2=$orders_obj->getWord("section_sys_messages", "system_message_id_2");

        require_once "../view/engine_orders.html";
        require_once "footer.php";
    }
    else{
        header("Location: login.php");
        exit;
    }
?>

