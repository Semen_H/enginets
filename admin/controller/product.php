<?php
    require_once "../model/Auth_Class.php";
    $user=Auth::getObject();
    $auth=$user->isAuth();
    if($auth){
        require_once "header.php";
        require_once "../model/product.php";

        $product_obj=new Product();
        $db_product_obj=new DB_Product();
        $product_id=$_GET["product_id"];//получаем id товара
        $delete_trigger=$_POST["delete_product"];//тригер удаления товара
        $delete_img=$_POST["delete_img"];//триггер удаления изображения


        $is_number=false;
        $product_info_array=false;
        $error=false;

        $is_product=$db_product_obj->isProduct($product_id);//проверяем,число ли GET-запрос
        if($is_product==true){
            $product_info_array=$db_product_obj->getProductInfo($product_id);//получаем массив с данными о товаре

            $product_name=$product_info_array["product_name"];//получаем название товара
            $product_id0=$product_info_array["product_id"];//получаем id товара
            $product_articul=$product_info_array["product_articul"];//получаем артикул товара
            $product_title_for_client=$product_info_array["product_title"];//получаем title товара
            $product_meta_description=$product_info_array["product_meta_description"];//получаем meta-description товара
            $product_post_meta_keywords=$product_info_array["product_meta_keywords"];//получаем meta-keywords товара
            $product_price=$product_info_array["product_price"];//получаем цену товара
            $product_img_url=$product_info_array["product_img_url"];//получаем урл картинки товара
            $product_br_description=$product_info_array["product_br_description"];//получаем краткое описание товара
            $product_full_description=$product_info_array["product_full_description"];//получаем полное описание товара
            $is_default_img=$product_obj->isDefaultImg($product_img_url);//проверяем на дефолтное изображение
        }
        else{
            $error=true;
        }

        //ajax удаление товара
        if($delete_trigger==true){
            $product_array=array($product_id);
            $rezult_delete=$db_product_obj->deleteProducts($product_array);
        }

        //ajax удаление изображения товара
        if($delete_img==true){
            $rezult_delete_img=$db_product_obj->deleteProductImg($product_id);
        }
        

        /*///////////////////////////////////////////
                    Переменные локализации
        ///////////////////////////////////////////*/
        $h1=$product_obj->getWord("add_product.php", "h1");



        require_once "../view/product.html";
        require_once "footer.php";
    }
    else{
        header("Location: login.php");
        exit;
    }
?>