<?php
    require_once "../model/header.php";

    $header_obj=new Header();
    $current_url=$header_obj->getCurrUrl();//сокращенный url
    $full_url=$header_obj->getFullPath();//полный url
    $default_img000=$header_obj->getDefaultImgUrl0();//путь к дефолтному изображению

    /*///////////////////////////////////////////
            Title зависимости от url
    ///////////////////////////////////////////*/
    switch($current_url){
        case "/admin/controller/login.php":
            $title=$header_obj->getWord("titles", "login.php");//читаем тайтл в секции pages с ключем Login в файле локализации
        break;
        case "/admin/controller/admin.php":
            $title=$header_obj->getWord("titles", "admin.php");
        break;
        case "/admin/controller/engine_orders.php":
            $title=$header_obj->getWord("titles", "engine_orders.php");
        break;
        case "/admin/controller/engine_products.php":
            $title=$header_obj->getWord("titles", "engine_products.php");
        break;
        case "/admin/controller/add_product.php":
            $title=$header_obj->getWord("titles", "add_product.php");
        break;
        case "/admin/controller/product.php":
            $title=$header_obj->getWord("titles", "product.php");
        break;
    }
     
    /*///////////////////////////////////////////
        Классы для body в зависимости от url
    ///////////////////////////////////////////*/
    $arr=array(
        '/admin/controller/admin.php'=>'admin',
        '/admin/controller/engine_orders.php'=>'orders',
        '/admin/controller/engine_products.php'=>'products',
        '/admin/controller/add_product.php'=>'add_product',
        '/admin/controller/product.php'=>'product'
    );
    $body_class=$arr[$current_url];
    if(array_key_exists($current_url, $arr)){
        $body_class;
    }
    $language_class=$header_obj->getLanguageClass(); //класс языка
    /*///////////////////////////////////////////
                CSS в зависимости от url
    ///////////////////////////////////////////*/
    switch($current_url){
        case "/admin/controller/login.php":
            $style=[
                1=>[
                   "href"=>"../view/components/bootstrap-3.2.0-dist/css/bootstrap.min.css",
                   "media"=>""
                ],
                2=>[
                   "href"=>"../view/components/bootstrap-3.2.0-dist/css/bootstrap-theme.min.css",
                   "media"=>""
                ],
                3=>[
                    "href"=>"../view/css/admin_style.css",
                    "media"=>"screen"
                ],
                4=>[
                    "href"=>"../view/components/font-awesome-4.5.0/css/font-awesome.min.css",
                    "media"=>""
                ]
            ];
        break;
        case "/admin/controller/admin.php":
            $style=[
                1=>[
                   "href"=>"../view/components/bootstrap-3.2.0-dist/css/bootstrap.min.css",
                   "media"=>""
                ],
                2=>[
                   "href"=>"../view/components/bootstrap-3.2.0-dist/css/bootstrap-theme.min.css",
                   "media"=>""
                ],
                3=>[
                    "href"=>"../view/css/admin_style.css",
                    "media"=>"screen"
                ],
                4=>[
                    "href"=>"../view/components/font-awesome-4.5.0/css/font-awesome.min.css",
                    "media"=>""
                ]
            ];
        break;
        case "/admin/controller/engine_orders.php":
            $style=[
                1=>[
                   "href"=>"../view/components/bootstrap-3.2.0-dist/css/bootstrap.min.css",
                   "media"=>""
                ],
                2=>[
                   "href"=>"../view/components/bootstrap-3.2.0-dist/css/bootstrap-theme.min.css",
                   "media"=>""
                ],
                3=>[
                    "href"=>"../view/css/admin_style.css",
                    "media"=>"screen"
                ],
                4=>[
                    "href"=>"../view/components/font-awesome-4.5.0/css/font-awesome.min.css",
                    "media"=>""
                ]
            ];
        break;
        case "/admin/controller/engine_products.php":
            $style=[
                1=>[
                   "href"=>"../view/components/bootstrap-3.2.0-dist/css/bootstrap.min.css",
                   "media"=>""
                ],
                2=>[
                   "href"=>"../view/components/bootstrap-3.2.0-dist/css/bootstrap-theme.min.css",
                   "media"=>""
                ],
                3=>[
                    "href"=>"../view/css/admin_style.css",
                    "media"=>"screen"
                ],
                4=>[
                    "href"=>"../view/components/font-awesome-4.5.0/css/font-awesome.min.css",
                    "media"=>""
                ]
            ];
        break;
        case "/admin/controller/add_product.php":
            $style=[
                1=>[
                   "href"=>"../view/components/bootstrap-3.2.0-dist/css/bootstrap.min.css",
                   "media"=>""
                ],
                2=>[
                   "href"=>"../view/components/bootstrap-3.2.0-dist/css/bootstrap-theme.min.css",
                   "media"=>""
                ],
                3=>[
                    "href"=>"../view/css/admin_style.css",
                    "media"=>"screen"
                ],
                4=>[
                    "href"=>"../view/components/font-awesome-4.5.0/css/font-awesome.min.css",
                    "media"=>""
                ]
            ];
        break;
        case "/admin/controller/product.php":
            $style=[
                1=>[
                   "href"=>"../view/components/bootstrap-3.2.0-dist/css/bootstrap.min.css",
                   "media"=>""
                ],
                2=>[
                   "href"=>"../view/components/bootstrap-3.2.0-dist/css/bootstrap-theme.min.css",
                   "media"=>""
                ],
                3=>[
                    "href"=>"../view/css/admin_style.css",
                    "media"=>"screen"
                ],
                4=>[
                    "href"=>"../view/components/font-awesome-4.5.0/css/font-awesome.min.css",
                    "media"=>""
                ],
                5=>[
                    "href"=>"../view/components/fancyBox/source/jquery.fancybox.css?v=2.1.5",
                    "media"=>"screen"
                ],
                6=>[
                    "href"=>"../view/components/fancyBox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5",
                    "media"=>"screen"
                ],
                7=>[
                    "href"=>"../view/components/fancyBox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7",
                    "media"=>"screen"
                ]
            ];
        break;
    }

    /*///////////////////////////////////////////
                JS в зависимости от url
    ///////////////////////////////////////////*/
    switch($current_url){
        case "/admin/controller/login.php":
            $js_header=[
                1=>[
                    "src"=>""
                ]
            ];
        break;
    }
        $login=$_SESSION["login"];//текущий пользователь
        /*///////////////////////////////////////////
                    Переменные локализации
        ///////////////////////////////////////////*/
        $greeting=$header_obj->getWord("header.php", "greeting");
        $see_site=$header_obj->getWord("header.php", "see_site");
        $logout=$header_obj->getWord("header.php", "logout");

        $menu=$header_obj->getWord("header.php", "menu_h");
        $menu_home=$header_obj->getWord("header.php", "menu_home");
        $menu_products=$header_obj->getWord("header.php", "menu_products");
        $menu_orders=$header_obj->getWord("header.php", "menu_orders");
        $menu_add_product=$header_obj->getWord("header.php", "menu_add_product");

        $w=$header_obj->getWord("header.php", "Correct_login_password");
        $w1=$header_obj->getWord("header.php", "is_empty");
        $w2=$header_obj->getWord("header.php", "select_one_order");
        $w3=$header_obj->getWord("header.php", "confirm_delete_orders");
        $w4=$header_obj->getWord("header.php", "confirm_delete_products");
        $w5=$header_obj->getWord("header.php", "select_one_product");


        /*///////////////////////////////////////////
                        Боковое меню
        ///////////////////////////////////////////*/

        $array_menu=[
            1=>[
                "href"=>"/admin/controller/admin.php",
                "title"=>"$menu_home",
                "class"=>"a_home",
                "name"=>"$menu_home"
            ],
            2=>[
                "href"=>"/admin/controller/engine_products.php?page=1",
                "title"=>"$menu_products",
                "class"=>"a_products",
                "name"=>"$menu_products"
            ],
            3=>[
                "href"=>"/admin/controller/engine_orders.php?page=1",
                "title"=>"$menu_orders",
                "class"=>"a_orders",
                "name"=>"$menu_orders"
            ],
            4=>[
                "href"=>"/admin/controller/add_product.php",
                "title"=>"$menu_add_product",
                "class"=>"a_add_product",
                "name"=>"$menu_add_product"
            ]
        ];

    require_once "../view/header.html";
?>