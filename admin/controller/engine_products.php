<?php
    require_once "../model/Auth_Class.php";
    $user=Auth::getObject();
    $auth=$user->isAuth();
    if($auth){
        $show_message=$_GET["show_message"];//���������� �� ���������
        $msg_id=$_GET["messages_id"];//id ���������
        $product_add_id=$_GET["product_add_id"];//������� id ������������ ������
        $product_delete_id000=$_GET["del_product_id"];//�������� id ���������� ������(� �������� ���������� ������) 

        require_once "header.php";
        require_once "../model/engine_products.php";

        $products_obj=new Engine_Products();
        $db_product_obj=new DB_Engine_Products();

        $full_patch=$products_obj->getFullPath();//�������� ������� ������ url
        $cut_patch=$products_obj->getCurrUrl();//�������� ������� ����������� url

        $get_page=$_GET["page"];//�������� get-������


        $count_posts=5;//���������� ������ �� ��������
        $is_number=preg_match("/^\d{1,}$/", $get_page);//����� �� get-������
        $delete_array1=$_POST["delete1"];//�������� ������ ��������� �������


        if($is_number){
            $count_all_posts_in_bd=$db_product_obj->getCountAllProducts();//�������� ���������� �������
            $array=$db_product_obj->getAllProducts($get_page, $count_posts);//�������� ��� ������
            $count_pages = ceil($count_all_posts_in_bd/$count_posts);//���������� ������� �����.
            $pg=$get_page;//������� ��������

            //���� �������� �������
            if($delete_array1) {//���� ���-���� ���� � �������(��� �������)
                session_start();
                $_SESSION["array_del_products"]=$delete_array1;

               $rezult_delete=$db_product_obj->deleteProducts($delete_array1);
               if($rezult_delete==true){ //���� ������� �������
                    header("Location: $cut_patch?page=1&show_message=true&messages_id=1");//�������� �� ������ ��������
               }
            }
           
            /* ������� ��������� */

            $active = $get_page;//������� �������� ��������.
            $count_show_pages = 5;// ���������� ������������ �������(��������� ���������)
            $url = "engine_products.php";//����� ��������, ��� ������� � �������� Pagination
            $url_page = "engine_products.php?page=";//���� �������� � ���������� page ��� �������� �� �����

            if ($count_pages > 1) { // �� ��� ������ ���� ���������� ������� ������ 1
                /* ������ ��� ���������� ������ ��������� �������� � ��������� (����� ������� �������� ���� ���-�� ���������, ���� ��� ��������, � ����� ����� ����� ��������� ������� ���� ����� count_show_pages, ���� ������, ���� ���������� ������� ������������) */
                $left = $active - 1;
                $right = $count_pages - $active;
                if ($left < floor($count_show_pages / 2)) $start = 1;
                else $start = $active - floor($count_show_pages / 2);
                $end = $start + $count_show_pages - 1;
                if ($end > $count_pages) {
                    $start -= ($end - $count_pages);
                    $end = $count_pages;
                    if ($start < 1) $start = 1;
                }
            }
        }
        /*///////////////////////////////////////////
                    ���������� �����������
        ///////////////////////////////////////////*/
        $h1=$products_obj->getWord("engine_products.php", "h1");
        $product_img_text=$products_obj->getWord("engine_products.php", "product_img_text");
        $product_name_text=$products_obj->getWord("engine_products.php", "product_name_text");
        $product_id_text=$products_obj->getWord("engine_products.php", "product_id_text");
        $product_description_text=$products_obj->getWord("engine_products.php", "product_description_text");
        $product_price_text=$products_obj->getWord("engine_products.php", "product_price_text");
        $all_products_text=$products_obj->getWord("engine_products.php", "all_products_text");
        $title_edit_product_text=$products_obj->getWord("engine_products.php", "title_edit_product_text");
        $look_product_text=$products_obj->getWord("engine_products.php", "look_product_text");
        $count_post_per_page=$products_obj->getWord("engine_products.php", "count_post_per_page");
        $page_per=$products_obj->getWord("engine_products.php", "page_per");
        $add_product_text=$products_obj->getWord("engine_products.php", "add_product_text");
        $product_articul_text=$products_obj->getWord("engine_products.php", "product_articul_text");

        //������ �����
        $select_all_text=$products_obj->getWord("section_options", "select_all_text");
        $confirm_button_text=$products_obj->getWord("section_options", "confirm_button_text");

        //pagination
        $first_page_text=$products_obj->getWord("section_pagination", "first_page_text");
        $previous_page_text=$products_obj->getWord("section_pagination", "previous_page_text");
        $current_page_text=$products_obj->getWord("section_pagination", "current_page_text");
        $next_page_text=$products_obj->getWord("section_pagination", "next_page_text");
        $last_page_text=$products_obj->getWord("section_pagination", "last_page_text");

        //messages
        $system_message_id_3=$products_obj->getWord("section_sys_messages", "system_message_id_3");
        $system_message_id_4=$products_obj->getWord("section_sys_messages", "system_message_id_4");
        $system_message_id_5=$products_obj->getWord("section_sys_messages", "system_message_id_5");
        $system_message_id_6=$products_obj->getWord("section_sys_messages", "system_message_id_9");
        $system_message_id_10=$products_obj->getWord("section_sys_messages", "system_message_id_10");

        require_once "../view/engine_products.html";
        require_once "footer.php";

    }
    else{
        header("Location: login.php");
        exit;
    }
?>