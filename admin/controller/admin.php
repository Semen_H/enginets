<?php
    require_once "../model/Auth_Class.php";
    $user=Auth::getObject();
    $auth=$user->isAuth();
    if($auth){

        require_once "header.php";
        require_once "../model/admin.php";

        $admin_obj=new Admin();
        $db_admin_obj=new DB_Admin();
        /*///////////////////////////////////////////
                    Переменные локализации
        ///////////////////////////////////////////*/
        $h1=$admin_obj->getWord("admin.php", "h1");
        $last_orders_text=$admin_obj->getWord("admin.php", "last_orders_text");
        $last_products_text=$admin_obj->getWord("admin.php", "last_products_text");
        $name_text=$admin_obj->getWord("admin.php", "last_orders_table_name");
        $phone_text=$admin_obj->getWord("admin.php", "last_orders_table_phone");
        $adress_text=$admin_obj->getWord("admin.php", "last_orders_table_adress");
        $comment_text=$admin_obj->getWord("admin.php", "last_orders_table_comments");
        $product_id_text=$admin_obj->getWord("admin.php", "last_orders_table_product_id");
        $order_id_text=$admin_obj->getWord("admin.php", "last_orders_table_order_id");
        $date_text=$admin_obj->getWord("admin.php", "last_orders_table_date");
        $status_text=$admin_obj->getWord("admin.php", "last_orders_table_status");
        $look_order_text=$admin_obj->getWord("admin.php", "last_orders_table_look_order");
        $look_all_order_text=$admin_obj->getWord("admin.php", "last_orders_table_look_all_order");
        $product_img_text=$admin_obj->getWord("admin.php", "product_img_text");
        $product_name_text=$admin_obj->getWord("admin.php", "product_name_text");
        $product_id_text=$admin_obj->getWord("admin.php", "product_id_text");
        $product_description_text=$admin_obj->getWord("admin.php", "product_description_text");
        $product_price_text=$admin_obj->getWord("admin.php", "product_price_text");
        $look_product_text=$admin_obj->getWord("admin.php", "look_product_text");
        $title_edit_product_text=$admin_obj->getWord("admin.php", "title_edit_product_text");
        $look_all_products_text=$admin_obj->getWord("admin.php", "look_all_products_text");
        $all_orders_text=$admin_obj->getWord("admin.php", "all_orders_text");
        $all_products_text=$admin_obj->getWord("admin.php", "all_products_text");

        $array=$db_admin_obj->getLastOrders(); //получаем массив с последними заказами из бд
        $array1=$db_admin_obj->getLastProducts(); //получаем массив с последними заказами из бд
        $count_all_orders=$db_admin_obj->getCountAllOrders();//получаем общее количество заказов
        $count_all_products=$db_admin_obj->getCountAllProducts();//получаем общее количество заказов

        require_once "../view/admin.html";
        require_once "footer.php";

    }
    else{
        header("Location: login.php");
        exit;
    }
?>

