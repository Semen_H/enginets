<?php
    require_once "Global_Class.php";
    require_once "DB_Class.php";

    class Admin extends Global_Class{
        /*
            Метод получения слова.
            Принимает имя секции, имя ключа из файла локализации.
            Возвращает строку.
        */
        public function getWord($section, $key){
             $word=parent::getWord($section, $key);
             return $word;
        }

        /*
            Метод конвертации unix время в человеко-понятное.
            Принимает формат даты, unix время.
            Возвращает строку-дату в заданном формате.
        */
        public function getTime($data_format, $unix_time){
            $rezult=date($data_format, $unix_time);
            return $rezult;
        }

    }
///////////////////////////////////////////////////////////////////////////////
    class DB_Admin extends DataBase{
        private $count_orders=5;//число отображаемых заказов
        private $order="DESC";//сортировка заказов
        private $order_by="id";//сортировка заказов по

        private $count_products=5;//число отображаемых товаров
        private $order_products="DESC";//сортировка товаров
        private $order_by_products="product_id";//сортировка товаров по

        public function __construct(){
            $this->db_prefix=parent::getDbPrefics();
            $this->db_connect=parent::getDB();
        }

        /*
            Метод получения последних заказов из БД.
            Возвращает число.
        */
        public function getLastOrders(){
            $query = "
                SELECT * FROM `".$this->db_prefix."orders` ORDER BY `{?}` {?} LIMIT {?}
            ";
            $returnable_array = $this->db_connect->select($query, array($this->order_by, $this->order, $this->count_orders));
            return $returnable_array;
        }

        /*
            Метод получения последних товаров из БД.
            Возвращает число.
        */
        public function getLastProducts(){
            $query = "
                SELECT `product_img_url`, `product_name`, `product_id`, `product_br_description`, `product_price`  FROM `".$this->db_prefix."products` ORDER BY `{?}` {?} LIMIT {?}
            ";
            $returnable_array = $this->db_connect->select($query, array($this->order_by_products, $this->order_products, $this->count_products));
            return $returnable_array;
        }

        /*
            Метод получения количества заказов из БД.
            Возвращает число.
        */
        public function getCountAllOrders(){
            $query = "
                SELECT `id` FROM `".$this->db_prefix."orders`
            ";
            $count = count($this->db_connect->select($query));
            return $count;
        }

        /*
            Метод получения количества товаров из БД.
            Возвращает число.
        */
        public function getCountAllProducts(){
            $query = "
                SELECT `product_id` FROM `".$this->db_prefix."products`
            ";
            $count = count($this->db_connect->select($query));
            return $count;
        }
    }
?>
