<?php
    require_once "Global_Class.php";
    require_once "DB_Class.php";

    class Add_Product extends Global_Class{
        /*
            Метод получения слова.
            Принимает имя секции, имя ключа из файла локализации.
            Возвращает строку.
        */
        public function getWord($section, $key){
             $word=parent::getWord($section, $key);
             return $word;
        }

        /*
            Метод конвертации unix время в человеко-понятное.
            Принимает формат даты, unix время.
            Возвращает строку-дату в заданном формате.
        */
        public function getTime($data_format, $unix_time){
            $rezult=date($data_format, $unix_time);
            return $rezult;
        }

        /*
            Метод получения URL с get запросом.
            Возвращает строку-полный url.
        */
        public function getFullPath(){
            $full_path=parent::getFullPath();
            return $full_path;
        }

        /*
            Метод получения текущего url.
            Возвращает строку с url.
        */
        public function getCurrUrl(){
            $curr_url=parent::getCurrUrl();
            return $curr_url;
        }

        /*
            Метод получения пути к картинкам из конфигов.
            Возвращает строку-путь.
        */
        public function getImgProductPatch(){
            $current_patch=parent::gerCurrentDomain()."/config/";
            $array=parse_ini_file($current_patch."config.ini", true); //учитывать разделы в конфигах
            $patch=$array["Theme_setup"]["img_sm_product_patch"];
            return $patch;
        }

    }
///////////////////////////////////////////////////////////////////////////////
    class DB_Add_Product extends DataBase{


        public function __construct(){
            $this->db_prefix=parent::getDbPrefics();
            $this->db_connect=parent::getDB();

        }

        public function addProduct($product_name, $articul,  $pr_title, $pr_meta_description, $keywords, $price, $product_br_description, $product_full_description, $img_name){
            $product_name=$this->realEscapeString($product_name);//название товара
            $articul=$this->realEscapeString($articul);//артикул товара
            $pr_title=$this->realEscapeString($pr_title);//title товара
            $pr_meta_description=$this->realEscapeString($pr_meta_description);//seo description
            $keywords=$this->realEscapeString($keywords);//ключевики
            $price=$this->realEscapeString($price);//цена
            $product_full_description=$this->realEscapeString($product_full_description);//полное описание
            $product_br_description=$this->realEscapeString($product_br_description);//краткое описание
            if($img_name){
                $img_url=$this->getProductsImgPatch().$img_name;
            }
            else{
                $img_url=$this->getProductsImgPatch()."nofoto.jpg";
            }

            $query="
                INSERT INTO `".$this->db_prefix."products` (
                    `product_name`,
                    `product_articul`,
                    `product_title`,
                    `product_meta_description`,
                    `product_meta_keywords`,
                    `product_price`,
                    `product_br_description`,
                    `product_full_description`,
                    `product_img_url`
                )
                VALUES (
                    '$product_name',
                    '$articul',
                    '$pr_title',
                    '$pr_meta_description',
                    '$keywords',
                    '$price',
                    '$product_br_description',
                    '$product_full_description',
                    '$img_url'
                )
            ";


            return $this->db_connect->query($query);
        }

        /*
            Метод экранирования ненужных символов для БД.
            Принимает строку.
            Возвращает строку-экранированный текст.
        */
        private function realEscapeString($string){
            return addslashes($string);
        }

        /*
            Метод получения урл загружаемых картинок.
            Возвращает строку-урл.
        */
        private function getProductsImgPatch(){
            $full_img_patch=parent::getUrlTheme()."view/images/products/";
            return $full_img_patch;
        }
    }
?>
