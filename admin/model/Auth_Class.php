<?php
    class Auth{
        private $db;
        private static $user=null;

        /*
            Метод получения массива конфигов базы.
            Возвращает массив из 4х значений.
        */
        private function getDbParametrs(){
            $full_ini_adress=$this->gerCurrentDomain()."/config/";
            $array=parse_ini_file($full_ini_adress."config.ini", true); //учитывать разделы в конфигах
            return $parametrs=[
                0=>$array["DB"]["db_host"],
                1=>$array["DB"]["db_user"],
                2=>$array["DB"]["db_password"],
                3=>$array["DB"]["db_name"]
            ];

        }

        /*
            Метод получения префикса базы из конфигов.
            Возвращает строку-префикс.
        */
        private function getDbPrefics(){
            $full_ini_adress=$this->gerCurrentDomain()."/config/";
            $array=parse_ini_file($full_ini_adress."config.ini", true); //учитывать разделы в конфигах
            return $db_prefix=$array["DB"]["db_prefix"];

        }

        /*
            Метод получения папки сайта.
            Возвращает строку.
        */
        private function gerCurrentDomain(){
            $curr_adress=$_SERVER['DOCUMENT_ROOT'];
            return $curr_adress;
        }

        private function __construct(){
            $this->dbconfig=$this->getDbParametrs();
            $this->db=new mysqli(
                $this->dbconfig[0],//хост
                $this->dbconfig[1],//пользователь
                $this->dbconfig[2],//пароль
                $this->dbconfig[3]//имя бд
            );
            $this->db->query("SET NAMES 'utf8'");
        }

        public static function getObject(){
            if(self::$user===null) self::$user=new Auth();
            return self::$user;
        }

        /*
            Метод проверки сущесвтования логина и пароля
        */
        private function checkUser($login, $password){
            $prefix=$this->getDbPrefics();
            $result_set=$this->db->query("
                SELECT `password` FROM `".$prefix."users` WHERE `login`='$login'
            ");
            $user=$result_set->fetch_assoc();
            $result_set->close();
            if(!$user) return false;
            return $user["password"]===$password;
        }

        /*
            Метод проверки входа пользователя
        */
        public function isAuth(){
            session_start();
            $login=$_SESSION["login"];
            $password=$_SESSION["password"];
            return $this->checkUser($login, $password);
        }

        /*
            Метод авторизации
        */
        public function login($login, $password){

            $password=md5($password);
            if($this->checkUser($login, $password)){
                session_start();
                $_SESSION["login"]=$login;
                $_SESSION["password"]=$password;
                return true;
            }
            else return false;
        }

        /*
            Метод закрывает соединение с базой данных
        */
        public function __descrust(){
            if($this->db) $this->db->close();
        }
    }
?>