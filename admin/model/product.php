<?php
    require_once "Global_Class.php";
    require_once "DB_Class.php";

    class Product extends Global_Class{
        /*
            ����� ��������� �����.
            ��������� ��� ������, ��� ����� �� ����� �����������.
            ���������� ������.
        */
        public function getWord($section, $key){
             $word=parent::getWord($section, $key);
             return $word;
        }

        /*
            ����� ����������� unix ����� � ��������-��������.
            ��������� ������ ����, unix �����.
            ���������� ������-���� � �������� �������.
        */
        public function getTime($data_format, $unix_time){
            $rezult=date($data_format, $unix_time);
            return $rezult;
        }

        /*
            ����� ��������� URL � get ��������.
            ���������� ������-������ url.
        */
        public function getFullPath(){
            $full_path=parent::getFullPath();
            return $full_path;
        }

        /*
            ����� ��������� �������� url.
            ���������� ������ � url.
        */
        public function getCurrUrl(){
            $curr_url=parent::getCurrUrl();
            return $curr_url;
        }

        /*
            ����� ��������� ���� � ��������� �� ��������.
            ���������� ������-����.
        */
        public function getImgProductPatch(){
            $current_patch=parent::gerCurrentDomain()."/config/";
            $array=parse_ini_file($current_patch."config.ini", true); //��������� ������� � ��������
            $patch=$array["Theme_setup"]["img_sm_product_patch"];
            return $patch;
        }

        /*
            ����� ��������� ������� ��� � ���������� ����������� �� ��������.
            ���������� ������-����.
        */
        public function getDefaultImgPatch(){
            $current_patch=parent::gerCurrentDomain()."/config/";
            $array=parse_ini_file($current_patch."config.ini", true); //��������� ������� � ��������
            $product_default_img=$array["Theme_setup"]["nofoto_patch"]; //�������� ������ ���� � ���������� �����������
            return $product_default_img;
        }

        /*
            ����� �������� �� ��������� ����������� ������.
            ��������� url �����������.
            ���������� true/false.
        */
        public function isDefaultImg($img_url){
            $product_default_img=$this->getDefaultImgPatch();
            if($img_url==$product_default_img) return true;
            return false;
        }
    }
///////////////////////////////////////////////////////////////////////////////
    class DB_Product extends DataBase{


        public function __construct(){
            $this->db_prefix=parent::getDbPrefics();
            $this->db_connect=parent::getDB();

        }

        public function getProductInfo($product_id){
            $query = "
                SELECT * FROM `".$this->db_prefix."products` WHERE `product_id`='".$product_id."'
            ";
            $returnable_array = $this->db_connect->selectRow($query);
            return $returnable_array;
        }

        public function updateProductInfo($product_name, $articul,  $pr_title, $pr_meta_description, $keywords, $price, $product_br_description, $product_full_description, $img_name){
            return $this->db_connect->query($query);
        }

        /*
            ����� ������������� �������� �������� ��� ��.
            ��������� ������.
            ���������� ������-�������������� �����.
        */
        private function realEscapeString($string){
            return addslashes($string);
        }

        /*
            ����� �������� �� ������������� ������.
            ��������� id ������.
            ����������� true � ������ ������. �����-false.
        */
        public function isProduct($product_id){
            $is_number=false;
            $is_number=$this->isNumber($product_id);

            $query = "
                SELECT `product_id` FROM `".$this->db_prefix."products` WHERE `product_id`='".$product_id."'
            ";
            $returnable_array = $this->db_connect->selectRow($query);
            if(($is_number==true) and (isset($returnable_array["product_id"]))) return true;
            else return false;

        }

        /*
            ��������������� ����� �������� ������ �� �����.
            ��������� ������.
            ���������� ���������(true/false).
        */
        private function isNumber($string){
            $rezult=preg_match("/^\d{1,}$/", $string);
            return $rezult;
        }

        /*
            ����� �������� ������� �� ��.
            ��������� ������ � ���������/���������� id �������.
            ���������� true � ������ ������, ����� false.
        */
        public function deleteProducts($delete_array){
            $query="DELETE FROM `".$this->db_prefix."products` WHERE `".$this->db_prefix."products`.`product_id` in(";
            //echo $query;
            for($i=0; $i<count($delete_array); $i++){
                $query.=$delete_array[$i];
                $query.=",";
            }
            $query=substr($query, 0, -1);//������� ��������� �������
            $query.=");";

            return $this->db_connect->query($query);
        }

        /*
            ����� ���������� ���������� ����������� � ������.
            ��������� �����-id ������.
            ���������� true/false.
        */
        public function deleteProductImg($product_id){
            $product_obj=new Product();
            $default_img_patch=$product_obj->getDefaultImgPatch();

           $query="
                UPDATE `".$this->db_prefix."products` SET
                    `product_img_url`='$default_img_patch'
                WHERE `product_id` ='$product_id';
            ";
            return $this->db_connect->query($query);
        }
    }
?>
