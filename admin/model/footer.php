<?php
    require_once "Global_Class.php";

    class Footer extends Global_Class{

        /*
            Метод получения текущего url.
            Возвращает строку с url.
        */
        public function getCurrUrl(){
            $curr_url=parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
            return $curr_url;
        }

        /*
            Метод получения слова.
            Принимает имя секции, имя ключа из файла локализации.
            Возвращает строку.
        */
        public function getWord($section, $key){
             $word=parent::getWord($section, $key);
             return $word;
        }
    }
?>