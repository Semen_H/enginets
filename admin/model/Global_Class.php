<?php
    class Global_Class{

        /*
            Метод получения слова из локализации.
            Принимает имя секции, имя ключа.
            Возвращает слово.
        */
        protected function getWord($section, $key){
            $lang_file_name=$this->parseLanguage();
            $arr=require $lang_file_name;
            return $arr[$section][$key];
        }

        /*
            Вспомогательный метод для getWord(); метод получает полный путь к текущему файлу локализации.
            /language/admin_x.php, где x-имя, сокращенный код языка, указанный в /config/config.ini.
            Возвращает строку-путь.
        */
        protected function parseLanguage(){
            $full_ini_adress=$this->gerCurrentDomain()."/config/";
            $array=parse_ini_file($full_ini_adress."config.ini", true); //учитывать разделы в конфигах
            $language=$array["Language"]["default_admin_language"];
            $lang_file_name=$this->setLanguageFile($language);//получаем полный путь к языковому файлу
            return $lang_file_name;
        }

        /*
            Вспомогательный метод для parseLanguage(); метод получения полного пути к файлу локализации.
            Принимает код языка.
            Возвращает строку-имя яз.файла.
        */
        protected function setLanguageFile($language){
            $lang_file_name = $this->gerCurrentDomain()."/admin/language/admin_".$language.".php";
            return $lang_file_name;
        }

        /*
            Вспомогательный метод для setLanguageFile(); метод получения пути к корневой папке домена.
            Возвращает строку-путь.
        */
        public function gerCurrentDomain(){
            $curr_adress=$_SERVER['DOCUMENT_ROOT'];
            return $curr_adress;
        }

        /*
            Метод получения URL с get запросом.
            Возвращает строку-полный url.
        */
        public function getFullPath(){
            $full_path=parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
            $array=parse_url($_SERVER['REQUEST_URI']);
            if($array["query"]) return $query=$full_path."?".$array["query"];
            else return $query=$full_path;
        }

        /*
            Метод получения текущего url.
            Возвращает строку с url.
        */
        public function getCurrUrl(){
            $curr_url=parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
            return $curr_url;
        }

    }
?>