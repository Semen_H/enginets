<?php
    require_once "Global_Class.php";

    class Header extends Global_Class{

        /*
            Метод получения title.
            Принимает имя секции, имя ключа из файла локализации.
            Возвращает строку-title.
        */
        public function getWord($section, $key){
             $title=parent::getWord($section, $key);
             return $title;
        }

        /*
            Метод получения текущего url.
            Возвращает строку с url.
        */
        public function getCurrUrl(){
            $curr_url=parent::getCurrUrl();
            return $curr_url;
        }

        /*
            Метод получения приставки языка с конфигов.
            Возвращает строку-приставку.
        */
        public function getLanguageClass(){
            $full_ini_adress=parent::gerCurrentDomain()."/config/";
            $array=parse_ini_file($full_ini_adress."config.ini", true); //учитывать разделы в конфигах
            $language=$array["Language"]["default_admin_language"];
            return $language;
        }

        /*
            Метод получения URL с get запросом.
            Возвращает строку-полный url.
        */
        public function getFullPath(){
            $full_path=parent::getFullPath();
            return $full_path;
        }

        /*
            Метод получения полного урл к дефолтному изображению из конфигов.
            Возвращает строку-путь.
        */
        public function getDefaultImgUrl0(){
            $current_patch=parent::gerCurrentDomain()."/config/";
            $array=parse_ini_file($current_patch."config.ini", true); //учитывать разделы в конфигах
            $product_default_img=$array["Theme_setup"]["nofoto_patch"]; //получаем полный путь к дефолтному изображению
            return $product_default_img;
        }
    }
?>