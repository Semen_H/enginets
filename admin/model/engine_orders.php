<?php
    require_once "Global_Class.php";
    require_once "DB_Class.php";

    class Engine_Orders extends Global_Class{
        /*
            Метод получения слова.
            Принимает имя секции, имя ключа из файла локализации.
            Возвращает строку.
        */
        public function getWord($section, $key){
             $word=parent::getWord($section, $key);
             return $word;
        }

        /*
            Метод конвертации unix время в человеко-понятное.
            Принимает формат даты, unix время.
            Возвращает строку-дату в заданном формате.
        */
        public function getTime($data_format, $unix_time){
            $rezult=date($data_format, $unix_time);
            return $rezult;
        }

        /*
            Метод получения URL с get запросом.
            Возвращает строку-полный url.
        */
        public function getFullPath(){
            $full_path=parent::getFullPath();
            return $full_path;
        }

        /*
            Метод получения текущего url.
            Возвращает строку с url.
        */
        public function getCurrUrl(){
            $curr_url=parent::getCurrUrl();
            return $curr_url;
        }

    }
///////////////////////////////////////////////////////////////////////////////
    class DB_Engine_Orders extends DataBase{
        private $order_by="id";
        private $order="DESC";

        public function __construct(){
            $this->db_prefix=parent::getDbPrefics();
            $this->db_connect=parent::getDB();
        }

        /*
            Метод выборки заказов из БД.
            Принимает страницу(число), и количество постов на страницу.
            Возвращает массив.
        */
        public function getAllOrders($get_page, $count_posts){
            $query="
                SELECT * FROM `".$this->db_prefix."orders` ORDER BY `{?}` {?} LIMIT ".(($get_page*$count_posts)-$count_posts).", $count_posts
            ";

            $returnable_array = $this->db_connect->select($query, array($this->order_by, $this->order));
            return $returnable_array;
        }

        /*
            Метод получения количества заказов из БД.
            Возвращает число.
        */
        public function getCountAllOrders(){
            $query = "
                SELECT `id` FROM `".$this->db_prefix."orders`
            ";
            $count = count($this->db_connect->select($query));
            return $count;
        }

        /*
            Метод удаления заказов из бд.
            Принимает массив с удаляемым/удаляемыми id заказов.
            Возвращает true в случае успеха, иначе false.
        */
        public function deleteOrders($delete_array){
            $query="DELETE FROM `".$this->db_prefix."orders` WHERE `".$this->db_prefix."orders`.`id` in(";

            for($i=0; $i<count($delete_array); $i++){
                $query.=$delete_array[$i];
                $query.=",";
            }
            $query=substr($query, 0, -1);//убираем последнюю запятую
            $query.=");";

            return $this->db_connect->query($query);
        }
    }
?>
