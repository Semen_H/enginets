<?php
    require_once "Global_Class.php";
    require_once "DB_Class.php";

    class Engine_Products extends Global_Class{
        /*
            ����� ��������� �����.
            ��������� ��� ������, ��� ����� �� ����� �����������.
            ���������� ������.
        */
        public function getWord($section, $key){
             $word=parent::getWord($section, $key);
             return $word;
        }

        /*
            ����� ����������� unix ����� � ��������-��������.
            ��������� ������ ����, unix �����.
            ���������� ������-���� � �������� �������.
        */
        public function getTime($data_format, $unix_time){
            $rezult=date($data_format, $unix_time);
            return $rezult;
        }

        /*
            ����� ��������� URL � get ��������.
            ���������� ������-������ url.
        */
        public function getFullPath(){
            $full_path=parent::getFullPath();
            return $full_path;
        }

        /*
            ����� ��������� �������� url.
            ���������� ������ � url.
        */
        public function getCurrUrl(){
            $curr_url=parent::getCurrUrl();
            return $curr_url;
        }

    }
///////////////////////////////////////////////////////////////////////////////
    class DB_Engine_Products extends DataBase{
        private $order_by="product_id";//���������� �� �����
        private $order="DESC";//������� ����������

        public function __construct(){
            $this->db_prefix=parent::getDbPrefics();
            $this->db_connect=parent::getDB();
        }

        /*
            ����� ������� ������� �� ��.
            ��������� ��������(�����), � ���������� ������ �� ��������.
            ���������� ������.
        */
        public function getAllProducts($get_page, $count_posts){
            $query="
                SELECT * FROM `".$this->db_prefix."products` ORDER BY `{?}` {?} LIMIT ".(($get_page*$count_posts)-$count_posts).", $count_posts
            ";
            $returnable_array = $this->db_connect->select($query, array($this->order_by, $this->order));
            return $returnable_array;
        }

        /*
            ����� ��������� ���������� ������� �� ��.
            ���������� �����.
        */
        public function getCountAllProducts(){
            $query = "
                SELECT `product_id` FROM `".$this->db_prefix."products`
            ";
            $count = count($this->db_connect->select($query));
            return $count;
        }

        /*
            ����� �������� ������� �� ��.
            ��������� ������ � ���������/���������� id �������.
            ���������� true � ������ ������, ����� false.
        */
        public function deleteProducts($delete_array){
            $query="DELETE FROM `".$this->db_prefix."products` WHERE `".$this->db_prefix."products`.`product_id` in(";

            for($i=0; $i<count($delete_array); $i++){
                $query.=$delete_array[$i];
                $query.=",";
            }
            $query=substr($query, 0, -1);//������� ��������� �������
            $query.=");";

            return $this->db_connect->query($query);
        }
    }
?>
